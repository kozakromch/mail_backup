package backup_crypto

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"golang.org/x/crypto/hkdf"
	"io"
	"os"
)

const SIZE_OF_SALT = 10
const SIZE_OF_HASH = 32


type User struct {
	UserName []byte         
	Salt     []byte             // crypto_random
	Password [SIZE_OF_HASH]byte // hash
}

type encryptedData struct {
	Nonce       []byte
	Cipher_text []byte
}

func CreateAndSaveNewUser(name, pass, file_name string) (user User, err error) {
	answer, err := isUserExist(name, file_name)
	if answer {
		err = errors.New("User with this name already exist")
		return
	}
	user, err = CreateNewUser(name, pass)
	if err != nil {
		return
	}

	appendEncryptedData(user, file_name)
	return
}
func CreateNewUser(name, pass string) (user User, err error) {
	user, err = encryptUser(name, pass)
	if err != nil {
		return
	}
	return
}

func isUserExist(name, file_name string) (answer bool, err error) {
	old_user, err := GetUser(name, file_name)
	if string(old_user.UserName) == name {
		return true, err
	}

	return false, err
}

func GetUser(name, file_name string) (user User, err error) {
	file, err := os.Open(file_name)
	if err != nil {
		return
	}
	defer file.Close()
	dec := json.NewDecoder(file)
	for {
		err := dec.Decode(&user)
		if err == io.EOF {
			err = nil
			break
		}

		if string(user.UserName) == name || err != nil {
			break
		}
	}
	return
}


func encryptUser(name, pass string) (user User, err error) {
	salt := make([]byte, SIZE_OF_SALT)
	_, err = io.ReadFull(rand.Reader, salt)
	if err != nil {
		return
	}

	hash_pass := makeHashPasswordWithSalt(pass, salt)
	byte_name := []byte(name)
	user = User{byte_name, salt, hash_pass}
	return
}

func makeHashPasswordWithSalt(pass string, salt []byte) (hash_pass [SIZE_OF_HASH]byte) {
	byte_pass := []byte(pass)
	hash_pass = sha256.Sum256(append(byte_pass, salt...))
	return 
}

func (u *User) AuthoriseUser(pass string) (answer bool) {
	hash_pass := makeHashPasswordWithSalt(pass, u.Salt)

	if u.Password != hash_pass {
		answer = false
	}
	answer = true
	return
}

func (u *User) ChangePassword(new_pass string) (err error) {
	salt := make([]byte, SIZE_OF_SALT)
	_, err = io.ReadFull(rand.Reader, salt)
	if err != nil {
		return
	}

	hash_pass := makeHashPasswordWithSalt(new_pass, salt)
	
	u.Password = hash_pass
	u.Salt = salt
	
	return
}

func (u *User) deleteFromFile(name, file_name string) (err error) {
	file, err := os.Open(file_name)
	if err != nil {
		return
	}
	defer file.Close()
	
	dec := json.NewDecoder(file)
	user := User{}
	
	for {
		err := dec.Decode(&user)
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			break
		}
		if string(u.UserName) == name {
			continue
		}
		appendEncryptedData(user, "tmpfile.iml")
	}
	if err != nil {
		return
	}

	err = os.Rename("tmpfile.iml", file_name)
	return
}

func writeNewEncryptedData(data interface{}, file_name string) (err error) {
	file, err := os.OpenFile(file_name, os.O_CREATE|os.O_WRONLY, 0644)
	
	defer file.Close()
	if err != nil {
		return
	}

	err = writeToFile(data, file)
	if err != nil {
		return
	}

	return
}

func appendEncryptedData(data interface{}, file_name string) (err error) {
	file, err := os.OpenFile(file_name, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	defer file.Close()
	if err != nil {
		return
	}

	err = writeToFile(data,file)
		if err != nil {
		return
	}
	
	return
}

func writeToFile(data interface{}, file *os.File) (err error) {
	json_data, err := json.Marshal(data)
	if err != nil {
		return
	}
	_, err = file.Write(append(json_data, []byte("\n")...))
	if err != nil {
		return
	}

	return
}

func (u *User) EncryptAndWriteData(data []byte, file_name string) (err error) {
	en_data, err := u.EncryptData(data)
	if err != nil {
		return
	}
	json_data, err := json.Marshal(en_data)
	if err != nil {
		return
	}
	err = writeNewEncryptedData(json_data, file_name)
	if err != nil {
		return
	}
	return
}

func (u *User) EncryptData(data []byte) (en_data encryptedData, err error) {
	aesgcm, err := u.makeAesgcm()
	if err != nil {
		return
	}

	//Одноразовый код. Не секретный. Может быть привязан к файлу
	nonce := make([]byte, aesgcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return
	}
	cipher_text := aesgcm.Seal(nil, nonce, data, nil)
	en_data = encryptedData{nonce, cipher_text}
	
	return
}

func (u *User) DecryptDataFromFile(file_name string) (data []byte, err error) {
	en_data, err := readEncryptedData(file_name)
	if err != nil {
		return
	}
	if en_data.Nonce == nil {
		return
	}

	return 
}

func (u *User) DecryptData(en_data encryptedData) (data []byte, err error) {
	aesgcm, err := u.makeAesgcm()
	if err != nil {
		return
	}

	data, err = aesgcm.Open(nil, en_data.Nonce, en_data.Cipher_text, nil)
	if err != nil {
		return
	}
	return
}

func readEncryptedData(file_name string) (en_data encryptedData, err error) {
	file, err := os.OpenFile(file_name, os.O_APPEND|os.O_CREATE|os.O_RDONLY, 0644)
	if err != nil {
		return
	}
	defer file.Close()

	dec := json.NewDecoder(file)
	err = dec.Decode(&en_data)
	if err == io.EOF {
		err = nil
		return
	}
	if err != nil {
		return
	}

	return
}

func (u *User) makeUserKey() (key []byte, err error) {
	hash := sha256.New

	hkdf := hkdf.New(hash, u.Password[:], u.Salt, nil)

	key = make([]byte, 32)
	_, err = io.ReadFull(hkdf, key)
	if err != nil {
		return
	}

	return
}

func (u *User) makeAesgcm() (aesgcm cipher.AEAD, err error) {
	key, err := u.makeUserKey()
	if err != nil {
		return
	}
	
	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	aesgcm, err = cipher.NewGCM(block)
	if err != nil {
		return
	}
	return
}