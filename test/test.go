package main

import (
	"bufio"
	"crypto_mail"
	"fmt"
	"folders_mail"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func cleanString(st string) string {
	return strings.ToLower(strings.Join(strings.Fields(st), " "))
}

func AddUser(reader *bufio.Reader) (err error) {
	fmt.Println("Create username")
	fmt.Print("add_user >>> ")

	username, err := reader.ReadString('\n')
	if err != nil {
		return
	}
	username = strings.TrimSpace(username)

	fmt.Println(username)
	fmt.Println("Create password")
	fmt.Print("add_user >>> ")
	password, err := reader.ReadString('\n')
	password = strings.TrimSpace(password)
	if err != nil {
		return
	}

	err = folders_mail.CreateNewUser(username, password)
	if err != nil {
		return
	}
	
	return
}

func SignIn(reader *bufio.Reader) (err error) {
	fmt.Println("Enter username")
	fmt.Print("sign_in >>> ")

	username, err := reader.ReadString('\n')
	if err != nil {
		return
	}
	username = strings.TrimSpace(username)

	fmt.Println("Enter password")
	fmt.Print("sign_in >>> ")

	password, err := reader.ReadString('\n')
	if err != nil {
		return
	}
	password = strings.TrimSpace(password)

	err = crypto_mail.CheckUser(username, password)
	if err != nil {
		return
	}
	user, err := crypto_mail.GetUser(username)
	if err != nil {
		return
	}
	AuthorizedUser(user, reader)
	return
}

func AuthorizedUser(user crypto_mail.UserStruct, reader *bufio.Reader) (err error) {
	fmt.Println("Enter command")

loop:
	for {

		fmt.Print(string(user.UserName), " >>> ")

		command, err := reader.ReadString('\n')
		if err != nil {
			break
		}
		command = cleanString(command)

		fmt.Println(command)

		switch command {
		case "add email":
			err = AddUserEmail(user, reader)
		case "change email":
			err = ChangeEmail(user, reader)
		case "delete email":
			err = DeleteUserEmail(user, reader)
		case "make backup":
			//err = MakeBackUp(user, reader)
		case "restore":
			//Restore(user, reader)
		case "change password":
			err = ChangeUserPassword(user, reader)
		case "help", "h":
			err = HelpAuth()
		case "quit", "exit":
			break loop
		default:
			fmt.Println("undefined command")
		}
		if err != nil {
			fmt.Println(err)
			break
		}
	}

	return
}

func HelpAuth() (err error) {
	fmt.Println(`
	Command:
		add email
		change email
		delete email
		make backup
		restore
		change password
		help/h
		quit/exit
	`)
	return
}

func AddUserEmail(user crypto_mail.UserStruct, reader *bufio.Reader) (err error) {

	email, err := requestDataEmail(reader, true)
	if err != nil {
		return
	}

	err = folders_mail.AddUserEmail(user, email)
	if err != nil {
		return
	}

	return
}

func ChangeEmail(user crypto_mail.UserStruct, reader *bufio.Reader) (err error) {
	email, err := choiceOfEmail(user, reader)
	fmt.Println("Press Enter if you want to keep data the same")
	new_email, err := requestDataEmail(reader, false)
	if err != nil {
		return
	}

	if new_email.Login == "" {
		new_email.Login = email.Login
	}
	if new_email.Password == "" {
		new_email.Password = email.Password
	}
	if new_email.ServerName == "" {
		new_email.ServerName = email.ServerName
	}
	if new_email.Port == 0 {
		new_email.Port = email.Port
	}

	err = folders_mail.DeleteUserEmail(user, email)
	if err != nil {
		return
	}
	err = folders_mail.AddUserEmail(user, email)
	if err != nil {
		return
	}

	return
}

func DeleteUserEmail(user crypto_mail.UserStruct, reader *bufio.Reader) (err error) {
	email, err := choiceOfEmail(user, reader)
	if err != nil {
		return
	}
	fmt.Println("Do you really want to delete email:", email.Login)
	fmt.Println("confirm yes/NO")
	confirm, err := reader.ReadString('\n')
		if err != nil {
		return
	}

	confirm = cleanString(confirm)
	if confirm == "yes" {
		err = folders_mail.DeleteUserEmail(user, email)
	}

	return

}

func ChangeUserPassword(user crypto_mail.UserStruct, reader *bufio.Reader) (err error) {
	fmt.Println("Enter new password")
	fmt.Print("sign_in >>> ")

	new_password_first, err := reader.ReadString('\n')
	if err != nil {
		return
	}
	new_password_first = strings.TrimSpace(new_password_first)

	fmt.Println("Enter new password again")
	fmt.Print("sign_in >>> ")

	new_password_second, err := reader.ReadString('\n')
	if err != nil {
		return
	}
	new_password_second = strings.TrimSpace(new_password_second)

	if new_password_first != new_password_second {
		return
	}

	fmt.Println("Enter new password again")
	fmt.Print("sign_in >>> ")
	password, err := reader.ReadString('\n')
	if err != nil {
		return
	}
	password = strings.TrimSpace(password)

	fmt.Println("confirm yes/NO")
	confirm, err := reader.ReadString('\n')
	if err != nil {
		return
	}
	confirm = cleanString(confirm)
	if confirm == "yes" {
		err = crypto_mail.ChangePsw(string(user.UserName), password, new_password_first)
	}

	return

}

func requestDataEmail(reader *bufio.Reader, check bool) (email folders_mail.EmailStruct, err error) {
	fmt.Println(`
	login
	password
	servername
	port uint, default 993
`)
	fmt.Println("Enter login")
	fmt.Printf("email >>> ")
	login, err := reader.ReadString('\n')
	if err != nil {
		return
	}
	login = strings.TrimSpace(login)
	//должно удовлетворять шаблону логина
	if check == true {
		var ch bool
		ch, err = regexp.MatchString(`\S+@\S+\.\S`, login)
		if err != nil {
			fmt.Println("HERE")
			return
		}
		if ch == false {
			fmt.Println("Wrong login")
			return
		}
	}
	fmt.Println("Enter password")
	fmt.Printf("email >>> ")
	password, err := reader.ReadString('\n')
	if err != nil {
		return
	}
	password = strings.TrimSpace(password)

	fmt.Println("Enter servername")
	fmt.Printf("email >>> ")
	server_name, err := reader.ReadString('\n')
	server_name = strings.TrimSpace(server_name)

	fmt.Println("Enter port number(default 993)")
	fmt.Printf("email >>> ")
	str_port, err := reader.ReadString('\n')
	if err != nil {
		return
	}
	str_port = cleanString(str_port)
	if check == true {
		var ch bool
		ch, err = regexp.MatchString(`^\d*$`, str_port)
		if err != nil {
			return
		}
		if ch == false {
			fmt.Println("Wrong port, should be a numeric")
			return
		}
	}
	port, err := strconv.Atoi(str_port)
	if err != nil {
		return
	}

	fmt.Println("confirm YES/no")
	fmt.Printf("email >>> ")
	confirm, err := reader.ReadString('\n')
		if err != nil {
		return
	}
	confirm = cleanString(confirm)
	if confirm == "no" {
		return
	}

	email = folders_mail.EmailStruct{login, password, server_name, port}

	return
}

func choiceOfEmail(user crypto_mail.UserStruct, reader *bufio.Reader) (email folders_mail.EmailStruct, err error) {
	emails, err := folders_mail.GetUserEmails(user)
	if err != nil {
		return
	}
	fmt.Println("Make a choice of number")
	fmt.Printf("email >>> ")
	for i, email := range emails {
		fmt.Println("[", i, "] ", email.Login)
	}
	str_number, err := reader.ReadString('\n')
		if err != nil {
		return
	}
	str_number = strings.TrimSpace(str_number)
	ch, err := regexp.MatchString(`\d`, str_number)
	if err != nil {
		return
	}
	if ch == false {
		fmt.Println("Wrong number, should be a numeric")
		return
	}

	number, err := strconv.Atoi(str_number)
	if err != nil {
		return
	}
	email = emails[number]
	return
}

func main() {
	reader := bufio.NewReader(os.Stdin)

loop:
	for {

		fmt.Print("main >>> ")

		command, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		command = cleanString(command)

		fmt.Println(command)

		switch command {
		case "add user":
			err = AddUser(reader)
		case "sign in", "log in":
			err = SignIn(reader)
		case "help", "h":
			err = HelpMain()
		case "quit", "exit":
			break loop
		default:
			fmt.Println("undefined command")
		}
		if err != nil {
			log.Println(err)
			break
		}
	}
	return
}

func HelpMain() (err error) {
	fmt.Println(`
Command:
	add user
	sign in/log in
	quit/exit
	`)
	return
}
