/*Код написан для создания back up-ов почты
*Подключаемся к почте, логируемся,
*просматриваем все папки выкачиваем все содержимое
*создаем два архива. 1)Полный в котором есть все письма и их параметры (зашифрован)
*2)Архив папки ящика + все UID которые там были + все флаги сообщения + uidValidity. 
*Возможно когда сообщения могут иметь одинаковый id в разных папках	 Но 
* когда diff-аем ящики по времени просмтариваем второй, при изменении закачиваем в первый
*При создании первого архива письмо читается полностью=>устанавливается флаг /read 
*Для всех непрочитанных писем восстанавливаем их флаги
*Добавить diff
*Добавить поиск сообщений в архиве
*Может вместо архива юзать базы данных ?
*Правила сархивации писем: Из каких папок выкачивать, даты, получатели,важные(по всем флагам) 
*Возможно что Папки отправленные, черновики, спам имеют одинаковый uidValidity отличаются только флаги у писем 
*/
package main

import (
	"log"
//	"fmt"
	//"mail_type"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-imap"
//	"encoding/json"
//	"os"
	"bytes"
)

type ShortMesInfo struct {
	Uid uint32 
	UidValidity uint32 
	flags []string
	folder string
}

const SERVER = "imap.yandex.ru:993"
const LOGIN = "ya.testgo@yandex.ru"
const PASSWORD = "testgo"

func connect_to_server(server string, login string, password string) (c *client.Client){
	log.Println("Connecting to server...")
	c, err := client.DialTLS(server, nil)
	log.Println("Connected")
	err = c.Login (login, password)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Logged in")
	return 
}
func main() {
			// section, err := imap.ParseBodySectionName("RFC822")
			// if err != nil {
			// 	log.Fatal(err)
			// }
			// log.Println("ready")

	//string of parameters responsible to download all info about the letter
	all_fields := []string{
			imap.EnvelopeMsgAttr,
			imap.UidMsgAttr,
			imap.FlagsMsgAttr,
			imap.InternalDateMsgAttr,
			imap.SizeMsgAttr,
			imap.BodyStructureMsgAttr,
			imap.BodyMsgAttr,
			"BODY[]"}
	c := connect_to_server(SERVER, LOGIN, PASSWORD)
	// List mailboxes
	mailboxes := make(chan *imap.MailboxInfo, 100)
	done := make(chan error, 1)
	go func () {
		done <- c.List("", "*", mailboxes)
	}()
	log.Println("GO")

//slices for info of message and folders contained it
//	folder_slice := make([]imap.MailboxStatus, 1)
//	mes_slice := make([]imap.Message, 1)
//	short_mes_slice := make([]ShortMesInfo, 1)

//Download all info from all folders
	//log.Println("Mailboxes:")
	for m := range mailboxes {

		//log.Println(m)

		// Select mailbox
		mbox, err := c.Select(m.Name, false)
		log.Println(m.Name)
		log.Println("::::::::::::::::::::::::::::::::::::::::::::::::")
		log.Println("::::::::::::::::::::::::::::::::::::::::::::::::")
		log.Println("::::::::::::::::::::::::::::::::::::::::::::::::")
		log.Println("::::::::::::::::::::::::::::::::::::::::::::::::")
		log.Println("UIDVALIDITY", mbox.UidValidity)
		log.Println("::::::::::::::::::::::::::::::::::::::::::::::::")
		log.Println("::::::::::::::::::::::::::::::::::::::::::::::::")
		log.Println("::::::::::::::::::::::::::::::::::::::::::::::::")
		log.Println("::::::::::::::::::::::::::::::::::::::::::::::::")
		if err != nil {
			log.Fatal(err)
		}
		//folder_slice = append(folder_slice, *mbox)
		//log.Println(m.Name, " ", mbox.UidValidity)

				from := uint32(1)
				to := uint32(2)
				uids := []uint32{2, 3, 4, 5, 6, 1}			
				seqset := new(imap.SeqSet)
				seqset.AddNum(uids...)
				seqset.AddRange(from, to)
				
				messages := make(chan *imap.Message, 10)
				done = make(chan error, 1)
				go func() {
					done <- c.Fetch(seqset, all_fields, messages)
				}()

				//log.Println("	messages:")
				
				for msg := range messages {

					//log.Println(msg)
					for  one, two := range msg.Body{
						log.Println(one)
						log.Println("--------------------------------------------")
						log.Println(two)
						log.Println("============================================")
					}

					log.Println("+++++++++++++++++++++++++++++++++++++++++++++++++")
					log.Println("+++++++++++++++++++++++++++++++++++++++++++++++++")
					log.Println("+++++++++++++++++++++++++++++++++++++++++++++++++")
					buf := new(bytes.Buffer)
					buf.ReadFrom(msg.GetBody("BODY[]"))
					log.Println(buf.String())
					log.Println("+++++++++++++++++++++++++++++++++++++++++++++++++")
					log.Println("+++++++++++++++++++++++++++++++++++++++++++++++++")
					log.Println("+++++++++++++++++++++++++++++++++++++++++++++++++")
					//mes_slice = append(mes_slice, *msg)
					// s_m := ShortMesInfo{Uid: msg.Uid, UidValidity: mbox.UidValidity, flags: msg.Flags, folder: mbox.Name}
					// short_mes_slice = append(short_mes_slice, s_m)
				}



		if err := <- done; err != nil {
			log.Fatal(err)
		}

	}

	// json_folder_slice, _ := json.Marshal(folder_slice)
	// fmt.Println(string(json_folder_slice))
	// buf := imap.MailboxStatus{}
	// _ = json.Unmarshal(json_folder_slice, &buf)
	// fmt.Println(buf	)
	// log.Println("\n\n")
	// json_short_slice, _ := json.Marshal(short_mes_slice)
	// log.Println(string(json_short_slice))
	// log.Println("Done!")

	// file_folder, err := os.Create("folders.iml")
	// file_short_mes, err := os.Create("short_mes.iml")

	// if err != nil {
	// 	log.Fatal(err)
	// }
	// defer file_folder.Close()
	// defer file_short_mes.Close()
	// _, err = file_folder.Write(json_short_slice)
	// _, err = file_short_mes.Write(json_folder_slice)

}

// buff :=  `Received: from mxback20j.mail.yandex.net (localhost.localdomain [127.0.0.1])
// 	by mxback20j.mail.yandex.net (Yandex) with ESMTP id 589632400D80;
// 	Thu, 31 Aug 2017 15:53:46 +0300 (MSK)
// Received: from web7o.yandex.ru (web7o.yandex.ru [95.108.205.107])
// 	by mxback20j.mail.yandex.net (nwsmtp/Yandex) with ESMTP id RvG3YCl8no-rjJWxsbs;
// 	Thu, 31 Aug 2017 15:53:46 +0300
// X-Yandex-Front: mxback20j.mail.yandex.net
// X-Yandex-TimeMark: 1504184026
// DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed; d=yandex.ru; s=mail; t=1504184026;
// 	bh=kaVlUfSET3xhOOtrhNi5+Cqx/35pavS9PU8AEK6AxDs=;
// 	h=From:To:Subject:Message-Id:Date;
// 	b=Bs3kBf321IUXtpBB5Mg46vd4+10RpqSSk9VA5DTyKEDndv7t/5ShxepDLdzMblU+S
// 	 xNLejrxympWVdDsyNULtQAUVJ7JlNp/CapaN5ZSyui0IhHR45P44njA0D4ezkSm8rx
// 	 taur73lFztHT5G+cH1n93A5vb/J56sLRM7XX4CUE=
// Authentication-Results: mxback20j.mail.yandex.net; dkim=pass header.i=@yandex.ru
// X-Yandex-Spam: 1
// X-Yandex-Sender-Uid: 539409045
// Received: by web7o.yandex.ru with HTTP;
// 	Thu, 31 Aug 2017 15:53:45 +0300
// From: Petr Petrov <ya.testgo1@yandex.ru>
// Envelope-From: ya-testgo1@yandex.ru
// To: ya.testgo@yandex.ru
// Subject: test5
// MIME-Version: 1.0
// Message-Id: <253811504184025@web7o.yandex.ru>
// X-Mailer: Yamail [ http://yandex.ru ] 5.0
// Date: Thu, 31 Aug 2017 15:53:45 +0300
// Content-Transfer-Encoding: 7bit
// Content-Type: text/html
// Return-Path: ya.testgo1@yandex.ru
// X-Yandex-Forward: c038913350a945b8fa17a4758183e5c3

// <div>DONT READ THIS MESSAGE. flag /seen must be unactive</div>`
// 		literal := bytes.NewBufferString(buff)

// 		c.Append("INBOX", []string{"//seen"}, time.Now(), )
