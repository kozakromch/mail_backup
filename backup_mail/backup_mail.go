package backup_mail

import (
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-imap"
	"time"
	"sort"
	"bytes"
)
type MailBackUp struct {
	UnixTimeStamp int32 
	Name string
	Mailboxes []Mailbox
	ShortMessages []ShortMessage //необходима для быстрого difa удаленных сообщений
}

type Mailbox struct {
	Name string
	Flags []string
	UidValidity uint32
	Messages []FullMessage
}

type FullMessage struct {
	Uid uint32
	Date time.Time
	Body string
	Flags []string
}

type ShortMessage struct {
	Uid uint32
	UidValidity uint32
	Flags []string
}


var FetchAllFields = []string{
			imap.EnvelopeMsgAttr,
			imap.UidMsgAttr,
			imap.FlagsMsgAttr,
			imap.InternalDateMsgAttr,
			imap.SizeMsgAttr,
			"BODY[]",}

var FetchShortFields = []string{
	imap.UidMsgAttr,
	imap.FlagsMsgAttr,
}
//сделать структуру backup
func MakeRootBackUp(name string, connection *client.Client) (backup MailBackUp, err error) {
	short_mes_slice, err := loadShortMessages(connection)
	if err != nil {
		return
	}

	mailboxes, err := loadMailboxesWithMessages(connection) 
	if err != nil {
		return
	}

	backup = MailBackUp{
		UnixTimeStamp: int32(time.Now().Unix()),
		Name: name,
		Mailboxes: mailboxes,
		ShortMessages: short_mes_slice,
	}
	return
}


// //считывает изменения с сервера тут же фиксирует это.
// //https://github.com/emersion/go-imap-idle
// func MakeIdleBackUp() () {

// }

// //backup по времени. 
// func MakeTimeBackUp() () {

// }
// //нужен демон https://github.com/takama/daemon  service in Windows /windows/svc/example

// //дает последний склееный бэкап как рутовый но свежий
// func getLastBackUp(name string) (backup MailBackUp, err error) {

// }
 
func MakeIncrementalBackUp(last_root_backup MailBackUp, connection *client.Client) (backup MailBackUp, err error) {
	part_of_backup, err := MakeDifferentialBackUp(last_root_backup, connection)
	if err != nil {
		return
	}
	var backup_slice []MailBackUp
	backup_slice = append(backup_slice, last_root_backup)
	backup = createFullBackUp(backup_slice)

	return
}

//собираем полный из кусков diffa и root
func createFullBackUp(backup_slice []MailBackUp) (full_backup MailBackUp) {
	sort.Slice(backup_slice, func(i, j int32) (bool){ return backup_slice[i].UnixTimeStamp > backup_slice[j].UnixTimeStamp })
	main_short_mes := backup_slice[0].ShortMessages
	var slice_mailboxes []Mailbox
	//backup в порядке возрастания. Выкачиваем все что можно опираясь на главный short_mes. То что выкачали удаляем из short_mes
	//
	for _, backup := range backup_slice {
		slice_mailboxes = append(slice_mailboxes, getMailboxesFromBackUp(backup, main_short_mes)...)
	}

	return
	//не обработана ситуация когда может быть одинаковые папки с разными сообщениями из разных времен
}

func getMailboxesFromBackUp(backup MailBackUp, short_mes_slice []ShortMessage) (slice_mailboxes []Mailbox) {
	for _, mbox := range backup.Mailboxes {
		var messages []FullMessage
			getMessagesFromMailbox(mbox, short_mes_slice)

		if len(messages) > 0 {
			buf_mailbox := Mailbox {
				Name: mbox.Name,
				Flags: mbox.Flags,
				UidValidity: mbox.UidValidity,
				Messages: messages,
			}

			slice_mailboxes = append(slice_mailboxes, buf_mailbox)			
		}	
	}

	return
}

func getMessagesFromMailbox(mbox Mailbox, short_mes_slice []ShortMessage) (messages []FullMessage) {	
	for i, short_mes := range short_mes_slice {
		if mbox.UidValidity == short_mes.UidValidity {
			mes := getMesFromMailbox(mbox, short_mes)
			if mes.Uid != 0 {
				messages = append(messages, mes)
				//удаляем выкаченное сообщение из short
				short_mes_slice = append(short_mes_slice[:i], short_mes_slice[i+1:]...)
			}
		}
	}

	return
}

func getMesFromMailbox(mbox Mailbox, short_mes ShortMessage) (mes FullMessage) {
	for _, mes := range mbox.Messages {
		if mes.Uid == short_mes.Uid {
			break 
		}
	}
	return 
}

func MakeDifferentialBackUp(last_backup MailBackUp, connection *client.Client) (backup MailBackUp, err error) {
	short_mes_slice, err := loadShortMessages(connection)
	if err != nil {
		return
	}

	need_to_load := getDifferentShortMessage(last_backup.ShortMessages, short_mes_slice)
	
	mailboxes, err := loadMailboxesWithMessagesByShortMessages(need_to_load, connection)
	if err != nil {
		return
	}

	backup = MailBackUp{
		UnixTimeStamp: int32(time.Now().Unix()),
		Name: last_backup.Name,
		Mailboxes: mailboxes,
		ShortMessages: short_mes_slice,
	}
	return
}

//сообщения с разными флагами считаем разными
func getDifferentShortMessage(old_short_mes, new_short_mes []ShortMessage) (short_mes []ShortMessage) {
	for _, mes := range new_short_mes {
		if !isShortMessageLocate(mes, old_short_mes) {
			short_mes = append (short_mes, mes)
		}
	}
	return 
}

func isShortMessageLocate(templ ShortMessage, messages [] ShortMessage) (answer bool) {
	for  _, mes := range messages {
		if templ.Uid == mes.Uid &&
			templ.UidValidity == mes.UidValidity &&
			isFlagsSame(templ.Flags, mes.Flags) {
			return true
		}
	}
	return false
}

func isFlagsSame(flags1, flags2 []string) (answer bool) {
	for _, flag := range flags1 {
		if !isStringLocate(flag, flags2) {
			return false
		}
	}
	return true
}

func isStringLocate(templ string, strings []string) (answer bool) {
	for _, str := range strings {
		if templ == str {
			return true
		}
	}
	return false
}

func loadShortMessages(connection *client.Client) (slice_shortmessages []ShortMessage, err error) {
	mailboxes_info, err := loadMailboxesInfo(connection)
	if err != nil {
		return
	}

	for _, m := range mailboxes_info {
		var slice_messages []imap.Message
		var mbox *imap.MailboxStatus
		slice_messages, mbox, err = loadMessagesAndStatus(m.Name, FetchShortFields, connection)
		if err != nil {
			break
		}

		for _, msg := range slice_messages {
			sm := ShortMessage{Uid: msg.Uid, UidValidity: mbox.UidValidity, Flags: msg.Flags}
			slice_shortmessages = append(slice_shortmessages, sm)
		}
	}
	return
}

func loadMailboxesWithMessagesByShortMessages(short_mes_slice []ShortMessage, connection *client.Client) (slice_mailboxes []Mailbox, err error) {
	mailboxes_info, err := loadMailboxesInfo(connection)
	if err != nil {
		return
	}

	for _, m := range mailboxes_info {
		mbox, err := connection.Select(m.Name, false)
		uids := getAllUidInUidValidity(short_mes_slice, mbox.UidValidity)
		if uids == nil {
			break
		}
		var slice_fullmessages []FullMessage
		slice_fullmessages, err = loadFullMessagesByUid(uids, connection)
		if err != nil {
			break
		}

		buf_mailbox := Mailbox {
			Name: m.Name,
			Flags: mbox.Flags,
			UidValidity: mbox.UidValidity,
			Messages: slice_fullmessages,

		}
		slice_mailboxes = append(slice_mailboxes, buf_mailbox)
	}

	return
}

func getAllUidInUidValidity(short_mes_slice []ShortMessage, UidValidity uint32) (uids []uint32) {
	for _, s_h := range short_mes_slice {
		if s_h.UidValidity == UidValidity {
			uids = append(uids, s_h.Uid)
		} 
	}
	return
}

func loadFullMessagesByUid(uids []uint32, connection *client.Client) (slice_fullmessages []FullMessage, err error) {
	seqset := new(imap.SeqSet)
	seqset.AddNum(uids...)
		
	messages := make(chan *imap.Message)
	done := make(chan error, 1)
	go func() {
		done <- connection.Fetch(seqset, FetchAllFields, messages)
	}()

	if err = <-done; err != nil {
		return
	}	
	var slice_messages []imap.Message
	for msg := range messages {
		slice_messages = append(slice_messages, *msg)
	}

	slice_fullmessages = loadFullMessages(slice_messages)
	
	return  
}

func loadMailboxesWithMessages(connection *client.Client) (slice_mailboxes []Mailbox, err error) {
	mailboxes_info, err := loadMailboxesInfo(connection)
	if err != nil {
		return
	}

	for _, m := range mailboxes_info {
		slice_messages, mbox, err := loadMessagesAndStatus(m.Name, FetchAllFields, connection)
		if err != nil {
			break
		}
		slice_fullmessages := loadFullMessages(slice_messages)
		buf_mailbox := Mailbox {
			Name: m.Name,
			Flags: mbox.Flags,
			UidValidity: mbox.UidValidity,
			Messages: slice_fullmessages,
		}
		slice_mailboxes = append(slice_mailboxes, buf_mailbox)
	}

	return
}

func loadFullMessages(slice_messages []imap.Message) (slice_fullmessages []FullMessage) {
	for _, msg := range slice_messages {
		buf := new(bytes.Buffer)
		buf.ReadFrom(msg.GetBody("BODY[]"))
		buf_msg := FullMessage{
			Uid: msg.Uid,
			Date: msg.Envelope.Date,
			Body: buf.String(),
			Flags: msg.Flags,
		}

		slice_fullmessages = append(slice_fullmessages, buf_msg)	
	}

	return
}

func loadMailboxesInfo(connection *client.Client) (mailboxes_info []*imap.MailboxInfo, err error) {
	mailboxes := make(chan *imap.MailboxInfo)
	
	done := make(chan error, 1)
	go func () {
		done <- connection.List("", "*", mailboxes)
	}()
	for m := range mailboxes {
		mailboxes_info = append(mailboxes_info, m)
	}

	if err = <-done; err != nil {
		return
	}

	return
}

func loadMessagesAndStatus(mailbox_name string, fetch_fields []string, connection *client.Client) (slice_messages []imap.Message, mbox *imap.MailboxStatus, err error) {
	mbox, err = connection.Select(mailbox_name, false)
	if err != nil {
		return
	}
	from := uint32(1)
	to := mbox.Messages

	seqset := new(imap.SeqSet)
	seqset.AddRange(from, to)
	
	messages := make(chan *imap.Message)
	done := make(chan error, 1)
	go func() {
		done <- connection.Fetch(seqset, fetch_fields, messages)
	}()

	if err = <-done; err != nil {
		return
	}	
	for msg := range messages {
		slice_messages = append(slice_messages, *msg)
	}
	return
}


// func getDifferentFlags(old_flags, new_flags []string) (flags_slice []string) {

// 	if old_flags == nil && new_flags == nil {
// 		return
// 	}
// 	for _, flag := range old_flags {
// 		if !isFlagsSame(flag, new_flags) {
// 			flags_slice = append(flag, flags_slice)
// 		}
// 	}
// 	return 
// }


// func isShortMessageSame(old_short_mes, new_short_mes []ShortMessage) (bool answer) {
// 	if old_short_mes == nil && new_short_mes == nil {
// 		return true
// 	}

// 	if old_short_mes == nil || new_short_mes == nil {
// 		return false
// 	}

// 	if len(old_short_mes) != len(new_short_mes) {
// 		return false
// 	}
// 	for _, i := range old_short_mes {
// 		if old_short_mes[i].Uid != new_short_mes[i].Uid ||
// 			 old_short_mes[i].UidValidity != new_short_mes.UidValidity ||
// 			 isFlagsSame(old_short_mes[i].Flags, ol) 
// 		return false
// 	}

// 	return true
// }

// func makeBackUp(connection *client.Client) (backup MailBackUp, err error) {
// 	mbox, err := connection.Select(m.Name, false)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	backup.Mailboxes = append(Mailboxes, *mbox)

// 	from := uint32(1)
// 	to := mbox.Messages

// 	seqset := new(imap.SeqSet)
// 	seqset.AddRange(from, to)
	
// 	messages := make(chan *imap.Message, 10)
// 	done = make(chan error, 1)
// 	go func() {
// 		done <- c.Fetch(seqset, all_fields, messages)
// 	}()

			
// 	for msg := range messages {
// 		mes_slice = append(mes_slice, *msg)
// 		s_m := ShortMesInfo{Uid: msg.Uid, UidValidity: mbox.UidValidity, flags: msg.Flags, folder: mbox.Name}
// 		short_mes_slice = append(short_mes_slice, s_m)
// 	}

// 	if err <-done; err != nil {
// 		log.Fatal(err)
// 	}
// }