package crypto_mail

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"golang.org/x/crypto/hkdf"
	"io"
	"os"
)

const SIZE_OF_SALT = 10
const SIZE_OF_HASH = 32

//
// Юзер создает акк в приложении. У него может быть несколько ящиков для бэкапа
//
type UserStruct struct {
	UserName []byte             // hash
	Salt     []byte             // crypto_random
	Password [SIZE_OF_HASH]byte // hash
}

type encryptedData struct {
	Nonce       []byte
	Cipher_text []byte
}

func CreateNewUser(name, pass string) (err error) {
	//Создаем нового юзера
	//Необходима проверка на существвание
	old_user, err := GetUser(name)
	if string(old_user.UserName) == name {
		err = errors.New("User with this name already exist")
		return
	}

	byte_name := []byte(name)
	byte_pass := []byte(pass)
	salt := make([]byte, SIZE_OF_SALT)
	_, err = io.ReadFull(rand.Reader, salt)
	if err != nil {
		return
	}

	hash_pass := sha256.Sum256(append(byte_pass, salt...))

	user := UserStruct{byte_name, salt, hash_pass}
	writeEncryptedData(user, "user.iml", true)
	return
}

func ChangePsw(name, pass, new_pass string) (err error) {
	//Меням пароль, предварительно запрашивая старый
	err = CheckUser(name, pass)
	if err != nil {
		return
	}
	err = deleteUser(name, pass)
	if err != nil {
		return
	}
	//может быть ситуация когда не удалось сохранить  нового
	err = CreateNewUser(name, new_pass)
	if err != nil {
		return
	}

	return
}

//возвращаем ошибку неверный логин или пароль
func CheckUser(name, pass string) (err error) {
	//Авторизируем пользователя
	byte_pass := []byte(pass)

	user, err := GetUser(name)
	if err != nil {
		return
	}

	hash_pass := sha256.Sum256(append(byte_pass, user.Salt...))

	if user.Password != hash_pass {
		err = errors.New("Wrong login or password")
	}

	return
}

//работа с файлами или базами данных (пока нерешено) ложится на get set
//not allow to use in another package
func GetUser(name string) (user UserStruct, err error) {
	file, err := os.Open("user.iml")
	if err != nil {
		return
	}
	defer file.Close()
	dec := json.NewDecoder(file)
	for {
		err := dec.Decode(&user)
		if err == io.EOF {
			err = nil
			break
		}

		if string(user.UserName) == name || err != nil {
			break
		}
	}
	return
}

func deleteUser(name, pass string) (err error) {
	file, err := os.Open("user.iml")
	if err != nil {
		return
	}
	defer file.Close()
	dec := json.NewDecoder(file)
	user := UserStruct{}
	for {
		err := dec.Decode(&user)
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			break
		}
		byte_pass := []byte(pass)
		hash_pass := sha256.Sum256(append(byte_pass, user.Salt...))
		if string(user.UserName) == name && user.Password == hash_pass {
			continue
		}
		writeEncryptedData(user, "tmpfile.iml", true)
	}
	err = os.Rename("tmpfile.iml", "user.iml")
	//err = os.Remove("tmpfile.iml")
	return
}

func writeEncryptedData(user interface{}, file_name string, append_flag bool) (err error) {
	var file *os.File
	if append_flag == true {
		file, err = os.OpenFile(file_name, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	} else {
		file, err = os.OpenFile(file_name, os.O_CREATE|os.O_WRONLY, 0644)
	}
	defer file.Close()
	if err != nil {
		return
	}

	json_user, err := json.Marshal(user)
	if err != nil {
		return
	}
	_, err = file.Write(append(json_user, []byte("\n")...))
	if err != nil {
		return
	}

	return
}

//
//
//

//
// Шифруем бэкапы и структуру логин пароль сервер
// Need to make a 32-byte key from password use hkdf
func makeUserKey(user UserStruct) (key []byte, err error) {
	//hkdf
	hash := sha256.New

	hkdf := hkdf.New(hash, user.Password[:], user.Salt, nil)

	key = make([]byte, 32)
	_, err = io.ReadFull(hkdf, key)
	if err != nil {
		return
	}

	return
}

func EncryptUserData(data []byte, user UserStruct, file_name string) (err error) {
	//aes
	key, err := makeUserKey(user)
	if err != nil {
		return
	}

	aesgcm, err := makeCipher(key)
	if err != nil {
		return
	}

	//Одноразовый код. Не секретный. Может быть привязан к файлу
	nonce := make([]byte, aesgcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return
	}
	cipher_text := aesgcm.Seal(nil, nonce, data, nil)
	en_data := encryptedData{nonce, cipher_text}

	writeEncryptedData(en_data, file_name, false)

	return
}

//запись в файл

func readEncryptedData(file_name string) (en_data encryptedData, err error) {
	file, err := os.OpenFile(file_name, os.O_APPEND|os.O_CREATE|os.O_RDONLY, 0644)
	if err != nil {
		return
	}
	defer file.Close()

	dec := json.NewDecoder(file)
	err = dec.Decode(&en_data)
	if err == io.EOF {
		err = nil
		return
	}
	if err != nil {
		return
	}

	return
}
func DecryptUserData(user UserStruct, file_name string) (data []byte, err error) {
	key, err := makeUserKey(user)
	if err != nil {
		return
	}

	aesgcm, err := makeCipher(key)
	if err != nil {
		return
	}

	en_data, err := readEncryptedData(file_name)
	if err != nil {
		return
	}
	if en_data.Nonce == nil {
		return
	}
	data, err = aesgcm.Open(nil, en_data.Nonce, en_data.Cipher_text, nil)
	if err != nil {
		return
	}
	return
}

func makeCipher(key []byte) (aesgcm cipher.AEAD, err error) {

	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	aesgcm, err = cipher.NewGCM(block)
	if err != nil {
		return
	}
	return
}



