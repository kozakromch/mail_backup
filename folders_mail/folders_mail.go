package folders_mail
//здесб вся привязка к конкретным файлам и emailam. Отсюда и вызываем
import (
	"bytes"
	"backup_crypto"
	"encoding/json"
	"io"
	"os"
	"path/filepath"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-imap"
	"backup_mail"
)

type Email struct {
	Login      string
	Password   string
	ServerName string
	Port       int
}

func CreateNewUser(name, password string) (err error) {
	err = os.Mkdir(name, os.ModePerm)
	if err != nil {
		return
	}
	_, err = backup_crypto.CreateAndSaveNewUser(name, password, "user.iml")
	if err != nil {
		return
	}

	return
}

func (e *Email) AddUserEmail(user backup_crypto.User, ) (err error) {
	path := filepath.Join(string(user.UserName), "data.iml")
	json_email, err := json.Marshal(e)
	if err != nil {
		return
	}
	err = encryptUserEmail(json_email, user, path)
	if err != nil {
		return
	}
	return
}

func encryptUserEmail(data []byte, user backup_crypto.User, filename string) (err error) {
	old_data, err := user.DecryptDataFromFile(filename)
	if err != nil {
		return
	}
	data = append(old_data, data...)
	err = user.EncryptAndWriteData(data, filename)
	if err != nil {
		return
	}
	return
}

func GetUserEmails(user backup_crypto.User) (emails []Email, err error) {
	path := filepath.Join(string(user.UserName), "data.iml")
	data, err := user.DecryptDataFromFile(path)
	if err != nil {
		return
	}
	reader := bytes.NewReader(data)
	email := Email{}
	dec := json.NewDecoder(reader)
	for {
		err := dec.Decode(&email)
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			break
		}
		emails = append(emails, email)
	}
	return
}

func (e* email) DeleteUserEmail(user backup_crypto.User) (err error) {

	path := filepath.Join(string(user.UserName), "data.iml")
	data, err := user.DecryptDataFromFile(path)
	if err != nil {
		return
	}
	reader := bytes.NewReader(data)
	buf_email := Email{}
	var json_emails []byte
	dec := json.NewDecoder(reader)
	for {
		err := dec.Decode(&buf_email)
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			break
		}
		if email.Login == buf_email.Login {
			continue
		}
		json_email, err := json.Marshal(e)
		if err != nil {
			break
		}
		json_emails = append(json_emails, json_email...)
	}
	err = user.EncryptAndWriteData(json_emails, path)
	if err != nil {
		return
	}

	return
}

func (e *Email) MakeAndSaveRootBackUp(user backup_crypto) (err error) {
	connection, err := e.connectToRemoteService()
	backup, err := backup_mail.MakeRootBackUp(e.Login, connection)
	
	name := backup.Name + backup.UnixTimeStamp

	path := filepath.Join(string(user.UserName), "backup", name)
	
	json_backup, err := json.Marshal(backup)
	err = user.EncryptAndWriteData(json_backup, path)
	if err != nil {
		return
	}
}


func (e* Email) connectToRemoteServer() (connection *client.Client, err error) {
	server := e.ServerName + ":" + string(e.Port)
	connection, err = client.DialTLS(server, nil)
	err = connection.Login (e.Login, e.Password)
	if err != nil {
		return
	}

	return 
}